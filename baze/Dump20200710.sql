CREATE DATABASE  IF NOT EXISTS `is_pametnakuca` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `is_pametnakuca`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: is_pametnakuca
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alarm`
--

DROP TABLE IF EXISTS `alarm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `alarm` (
  `idalarm` int(11) NOT NULL AUTO_INCREMENT,
  `idKorisnik` int(11) NOT NULL,
  `vreme` datetime NOT NULL,
  `period` bigint(50) NOT NULL,
  PRIMARY KEY (`idalarm`),
  KEY `alarm_idKorisnik_FK_idx` (`idKorisnik`),
  CONSTRAINT `alarm_idKorisnik_FK` FOREIGN KEY (`idKorisnik`) REFERENCES `korisnik` (`idKorisnik`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alarm`
--

LOCK TABLES `alarm` WRITE;
/*!40000 ALTER TABLE `alarm` DISABLE KEYS */;
INSERT INTO `alarm` VALUES (0,1,'2020-07-06 18:37:54',5),(13,1,'2020-07-06 19:00:00',-1),(14,1,'2020-07-06 19:00:00',-1),(15,1,'2020-07-06 19:00:00',10),(16,1,'2020-07-06 19:00:00',20),(17,1,'2020-07-06 19:00:00',20),(18,2,'2020-07-06 19:00:00',10),(19,1,'2020-07-08 22:46:00',-1),(20,1,'2020-07-08 22:47:00',15),(21,2,'2020-07-08 23:11:01',-1),(22,2,'2020-07-08 23:11:32',-1),(23,1,'2020-07-09 15:17:59',-1),(24,1,'2020-07-09 14:04:09',-1),(25,1,'2020-07-09 15:00:09',-1),(26,1,'2020-07-09 16:00:09',-1);
/*!40000 ALTER TABLE `alarm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `korisnik` (
  `idKorisnik` int(11) NOT NULL AUTO_INCREMENT,
  `korisnickoIme` varchar(50) NOT NULL,
  `sifra` varchar(50) NOT NULL,
  `idPesma` int(11) NOT NULL DEFAULT '1',
  `koordX` bigint(50) NOT NULL,
  `koordY` bigint(50) NOT NULL,
  PRIMARY KEY (`idKorisnik`),
  UNIQUE KEY `korisnickoIme_UNIQUE` (`korisnickoIme`),
  KEY `korisnik_idPesma_FK_idx` (`idPesma`),
  CONSTRAINT `korisnik_idPesma_FK` FOREIGN KEY (`idPesma`) REFERENCES `pesma` (`idpesma`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES (1,'admin','123',4,100,100),(2,'laza','123',3,120,120),(3,'pera','123',1,140,140);
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obaveza`
--

DROP TABLE IF EXISTS `obaveza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `obaveza` (
  `idobaveza` int(11) NOT NULL AUTO_INCREMENT,
  `idKorisnik` int(11) NOT NULL,
  `pocetak` datetime NOT NULL,
  `trajanje` bigint(50) NOT NULL,
  `koordX` bigint(50) NOT NULL,
  `koordY` bigint(50) NOT NULL,
  PRIMARY KEY (`idobaveza`),
  KEY `obaveza_idKorisnik_FK_idx` (`idKorisnik`),
  CONSTRAINT `obaveza_idKorisnik_FK` FOREIGN KEY (`idKorisnik`) REFERENCES `korisnik` (`idKorisnik`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obaveza`
--

LOCK TABLES `obaveza` WRITE;
/*!40000 ALTER TABLE `obaveza` DISABLE KEYS */;
INSERT INTO `obaveza` VALUES (1,2,'2020-07-08 17:00:00',1800,100,130),(3,2,'2020-07-08 19:30:00',3600,130,125),(6,1,'2020-07-09 17:25:00',1700,105,105),(7,3,'2020-07-09 16:10:00',1800,140,140),(12,3,'2020-07-09 08:08:00',3600,140,140);
/*!40000 ALTER TABLE `obaveza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pesma`
--

DROP TABLE IF EXISTS `pesma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pesma` (
  `idpesma` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `naziv` varchar(50) NOT NULL,
  PRIMARY KEY (`idpesma`),
  UNIQUE KEY `url_UNIQUE` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pesma`
--

LOCK TABLES `pesma` WRITE;
/*!40000 ALTER TABLE `pesma` DISABLE KEYS */;
INSERT INTO `pesma` VALUES (1,'https://www.youtube.com/watch?v=HOZW3yQxexc','Dawn Chorus'),(2,'https://www.youtube.com/watch?v=Ah7kOAm_UfU','Nightsky'),(3,'https://www.youtube.com/watch?v=rNWzrcb987M','Morning Flower'),(4,'https://www.youtube.com/watch?v=Lkfr3dnNOAY','Over the Horizon');
/*!40000 ALTER TABLE `pesma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pretraga`
--

DROP TABLE IF EXISTS `pretraga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pretraga` (
  `idpretraga` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(100) NOT NULL,
  `idKorisnik` int(11) NOT NULL,
  PRIMARY KEY (`idpretraga`),
  KEY `pretraga_idKorisnik_FK_idx` (`idKorisnik`),
  CONSTRAINT `pretraga_idKorisnik_FK` FOREIGN KEY (`idKorisnik`) REFERENCES `korisnik` (`idKorisnik`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pretraga`
--

LOCK TABLES `pretraga` WRITE;
/*!40000 ALTER TABLE `pretraga` DISABLE KEYS */;
INSERT INTO `pretraga` VALUES (1,'pesma',1),(3,'react',1),(6,'react',2),(15,'familiar',2),(17,'numa numa',2),(18,'numa numa',1),(20,'milimetar',1),(22,'numa numa',3),(24,'slon lepotan',3);
/*!40000 ALTER TABLE `pretraga` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-10 14:17:52
