package is_planer;

import entities.Korisnik;
import entities.Obaveza;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;


public class Main {
    
    private static EntityManager em = Persistence.createEntityManagerFactory("IS_PlanerPU").createEntityManager();

    @Resource(lookup = "jms/__defaultConnectionFactory")
    private static ConnectionFactory connectionFactory;
    
    @Resource(lookup = "my_planner_queue")
    private static Queue plannerQueue;
    
    @Resource(lookup = "my_server_topic")
    private static Topic serverTopic;
    
    private static long SPEED_KM_PER_H = 5;
    
    private static String getDate(Date datum){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.HH:mm:ss");
        return sdf.format(datum);
    }
    
    private static double euclideanDistance(Obaveza o1, Obaveza o2) {
        return Math.sqrt(Math.pow(o1.getKoordX() - o2.getKoordX(), 2) + Math.pow(o1.getKoordY() - o2.getKoordY(), 2));
    }
    
    private static double calcTravellingTimeInSeconds(Obaveza o1, Obaveza o2){
        return euclideanDistance(o1, o2) * 60 * 60 / (SPEED_KM_PER_H * 1.0);
    }
    
    private static boolean kalkulator(int idKorisnik, Obaveza obaveza){
        List<Obaveza> obaveze = em.createQuery("SELECT o FROM Obaveza o WHERE o.idKorisnik.idKorisnik = :idKorisnik ORDER BY o.pocetak", Obaveza.class)
                .setParameter("idKorisnik", idKorisnik).getResultList();
        
        Obaveza pre = null, posle = null;
        for (Obaveza o: obaveze){
            if (o.getIdobaveza().equals(obaveza.getIdobaveza()))
                continue;
            if (o.getPocetak().before(obaveza.getPocetak()))
                pre = o;
            else {
                posle = o;
                break;
            }
        }
        
        long pocetakO = obaveza.getPocetak().getTime();
        long krajO = pocetakO + (obaveza.getTrajanje()*1000);
        
        if (pre != null){
            long krajPre = pre.getPocetak().getTime() + (pre.getTrajanje()*1000);
            double travellingTime = calcTravellingTimeInSeconds(pre, obaveza);
            
            if ((krajPre + (travellingTime * 1000)) > pocetakO)
                return false;
        } 
        if (posle != null){
            long pocetakPosle = posle.getPocetak().getTime();
            double travellingTime = calcTravellingTimeInSeconds(obaveza, posle);
            
            if ((krajO + (travellingTime * 1000)) > pocetakPosle)
                return false;
            
        }
        
        return true;
    }
    
    private static void dodajObavezu(JMSContext context, int idKorisnik, Date pocetak, long trajanje, long koordX, long koordY){
        Obaveza obaveza = new Obaveza();
        Korisnik korisnik = em.find(Korisnik.class, idKorisnik);
        obaveza.setIdKorisnik(korisnik);
        obaveza.setPocetak(pocetak);
        obaveza.setTrajanje(trajanje);
        if (koordX == -1 || koordY == -1){
            obaveza.setKoordX(korisnik.getKoordX());
            obaveza.setKoordY(korisnik.getKoordY());
        } else {
            obaveza.setKoordX(koordX);
            obaveza.setKoordY(koordY);
        }
        if (!kalkulator(idKorisnik, obaveza)){
            posaljiNeuspeh(context, idKorisnik);
            return;
        }
        em.getTransaction().begin();
        em.persist(obaveza);
        em.getTransaction().commit();
        
        System.out.println("Obaveza: " + getDate(pocetak));       
    }
    
    private static void izmeniObavezu(JMSContext context, int idKorisnik, Date pocetak, long trajanje, long koordX, long koordY, int idObaveza){
        Obaveza obaveza = em.find(Obaveza.class, idObaveza);
        if (obaveza == null)
            return;
        Korisnik korisnik = em.find(Korisnik.class, idKorisnik);
        if (!getDate(pocetak).equals("00.00.0000.00:00:00"))
            obaveza.setPocetak(pocetak);
        if (trajanje != -2)
            obaveza.setTrajanje(trajanje);
        if (koordX != -2 && koordY != -2){
            if (koordX == -1 || koordY == -1){
                obaveza.setKoordX(korisnik.getKoordX());
                obaveza.setKoordY(korisnik.getKoordY());
            } else {
                obaveza.setKoordX(koordX);
                obaveza.setKoordY(koordY);
            }
        }
        if (!kalkulator(idKorisnik, obaveza)){
            posaljiNeuspeh(context, idKorisnik);
            return;
        }
        em.getTransaction().begin();
        em.persist(obaveza);
        em.getTransaction().commit();
        
        System.out.println("Obaveza: " + getDate(pocetak));       
    }
    
    private static void obrisiObavezu(int idKorisnik, int idObaveza){
        Obaveza obaveza = em.find(Obaveza.class, idObaveza);
        if (obaveza == null)
            return;
        if (obaveza.getIdKorisnik().getIdKorisnik() == idKorisnik){
            em.getTransaction().begin();
            em.remove(obaveza);
            em.getTransaction().commit();

            System.out.println("Obaveza obrisana: " + idObaveza);
        } else
            System.err.println("Obaveza se ne moze obrisati jer korisnik nije njen vlasnik.");
    }
    
    private static void posaljiNeuspeh(JMSContext context, int idKorisnik){        
        try {
            JMSProducer producer = context.createProducer();
            TextMessage textMessage = context.createTextMessage("Neuspeh");
            textMessage.setIntProperty("korisnik", idKorisnik);
            textMessage.setStringProperty("tip", "neuspelaObaveza");
            producer.send(serverTopic, textMessage);
            
        } catch (JMSException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void posaljiObaveze(JMSContext context, int idKorisnik){        
        List<Obaveza> obaveze = em.createQuery("SELECT o FROM Obaveza o WHERE o.idKorisnik.idKorisnik = :idKorisnik", Obaveza.class)
                .setParameter("idKorisnik", idKorisnik).getResultList();
        
        StringBuilder sb = new StringBuilder();
        obaveze.forEach((p) -> {
            sb.append("[").append(p.getIdobaveza()).append("] ").append(getDate(p.getPocetak())).append(" - ").append(p.getTrajanje()).
                    append("s (").append(p.getKoordX()).append(",").append(p.getKoordY()).append(")").append("\n");
        });
        
        try {
            JMSProducer producer = context.createProducer();
            TextMessage textMessage = context.createTextMessage(sb.toString());
            textMessage.setIntProperty("korisnik", idKorisnik);
            textMessage.setStringProperty("tip", "obaveze");
            producer.send(serverTopic, textMessage);
            
        } catch (JMSException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void posaljiVremePolaska(JMSContext context, int idKorisnik, int idObaveza){        
        List<Obaveza> temp = em.createQuery("SELECT o FROM Obaveza o WHERE o.idobaveza = :idObaveza AND o.idKorisnik.idKorisnik = :idKorisnik", Obaveza.class)
                .setParameter("idObaveza", idObaveza).setParameter("idKorisnik", idKorisnik).getResultList();
        Korisnik korisnik = em.find(Korisnik.class, idKorisnik);
        
        if (temp == null || temp.isEmpty() || korisnik == null)
            return;
        
        Obaveza obaveza = temp.get(0);
        
        
        List<Obaveza> obaveze = em.createQuery("SELECT o FROM Obaveza o WHERE o.idKorisnik.idKorisnik = :idKorisnik AND o.idobaveza != :idObaveza ORDER BY o.pocetak", Obaveza.class)
                .setParameter("idKorisnik", idKorisnik).setParameter("idObaveza", obaveza.getIdobaveza()).getResultList();
        
        Obaveza pre = null;
        for (Obaveza o: obaveze){
            if (o.getPocetak().before(obaveza.getPocetak()))
                pre = o;
            else
                break;
        }
        
        long pocetakO = obaveza.getPocetak().getTime();
        double travellingTime;
        
        if (pre != null)
            travellingTime = calcTravellingTimeInSeconds(pre, obaveza);
        else 
            travellingTime = calcTravellingTimeInSeconds(new Obaveza(-1, null, -1, korisnik.getKoordX(), korisnik.getKoordY()), obaveza);
        
        Date vremePolaska = new Date(pocetakO - ((long) travellingTime*1000));
        
        try {
            JMSProducer producer = context.createProducer();
            TextMessage textMessage = context.createTextMessage(getDate(vremePolaska));
            textMessage.setIntProperty("korisnik", idKorisnik);
            textMessage.setStringProperty("tip", "vremePolaska");
            producer.send(serverTopic, textMessage);
            
        } catch (JMSException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        JMSContext context = connectionFactory.createContext();
        JMSConsumer consumer = context.createConsumer(plannerQueue);
        
        while(true){
            Message message = consumer.receive();
            if(message instanceof TextMessage){
                try {
                    TextMessage textMessage = (TextMessage) message;
                    int idKorisnik = textMessage.getIntProperty("korisnik");
                    int idObaveza = textMessage.getIntProperty("idObaveza");
                    boolean zahtevZaVremePolaska = textMessage.getBooleanProperty("zahtevZaVremePolaska");
                    System.out.println("Primljena je poruka sa sadrzajem: " + idObaveza);
                    
                    if (idObaveza == -1)
                        posaljiObaveze(context, idKorisnik);
                    else if (zahtevZaVremePolaska)
                        posaljiVremePolaska(context, idKorisnik, idObaveza);
                    else
                        obrisiObavezu(idKorisnik, idObaveza);
                    
                } catch (JMSException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (message instanceof ObjectMessage){
                try {
                    ObjectMessage objectMessage = (ObjectMessage) message;
                    int idKorisnik = objectMessage.getIntProperty("korisnik");
                    Date pocetak = (Date) objectMessage.getObject();
                    long trajanje = objectMessage.getLongProperty("trajanje");
                    long koordX = objectMessage.getLongProperty("koordX");
                    long koordY = objectMessage.getLongProperty("koordY");
                    int idObaveza = objectMessage.getIntProperty("idObaveza");
                    
                    System.out.println("Primljena je poruka sa sadrzajem: " + getDate(pocetak));
                    if (idObaveza == -1)
                        dodajObavezu(context, idKorisnik, pocetak, trajanje, koordX, koordY);
                    else
                        izmeniObavezu(context, idKorisnik, pocetak, trajanje, koordX, koordY, idObaveza);
                    
                } catch (JMSException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
}
