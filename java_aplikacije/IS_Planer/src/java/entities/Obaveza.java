/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "obaveza")
@NamedQueries({
    @NamedQuery(name = "Obaveza.findAll", query = "SELECT o FROM Obaveza o"),
    @NamedQuery(name = "Obaveza.findByIdobaveza", query = "SELECT o FROM Obaveza o WHERE o.idobaveza = :idobaveza"),
    @NamedQuery(name = "Obaveza.findByPocetak", query = "SELECT o FROM Obaveza o WHERE o.pocetak = :pocetak"),
    @NamedQuery(name = "Obaveza.findByTrajanje", query = "SELECT o FROM Obaveza o WHERE o.trajanje = :trajanje"),
    @NamedQuery(name = "Obaveza.findByKoordX", query = "SELECT o FROM Obaveza o WHERE o.koordX = :koordX"),
    @NamedQuery(name = "Obaveza.findByKoordY", query = "SELECT o FROM Obaveza o WHERE o.koordY = :koordY")})
public class Obaveza implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idobaveza")
    private Integer idobaveza;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pocetak")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pocetak;
    @Basic(optional = false)
    @NotNull
    @Column(name = "trajanje")
    private long trajanje;
    @Basic(optional = false)
    @NotNull
    @Column(name = "koordX")
    private long koordX;
    @Basic(optional = false)
    @NotNull
    @Column(name = "koordY")
    private long koordY;
    @JoinColumn(name = "idKorisnik", referencedColumnName = "idKorisnik")
    @ManyToOne(optional = false)
    private Korisnik idKorisnik;

    public Obaveza() {
    }

    public Obaveza(Integer idobaveza) {
        this.idobaveza = idobaveza;
    }

    public Obaveza(Integer idobaveza, Date pocetak, long trajanje, long koordX, long koordY) {
        this.idobaveza = idobaveza;
        this.pocetak = pocetak;
        this.trajanje = trajanje;
        this.koordX = koordX;
        this.koordY = koordY;
    }

    public Integer getIdobaveza() {
        return idobaveza;
    }

    public void setIdobaveza(Integer idobaveza) {
        this.idobaveza = idobaveza;
    }

    public Date getPocetak() {
        return pocetak;
    }

    public void setPocetak(Date pocetak) {
        this.pocetak = pocetak;
    }

    public long getTrajanje() {
        return trajanje;
    }

    public void setTrajanje(long trajanje) {
        this.trajanje = trajanje;
    }

    public long getKoordX() {
        return koordX;
    }

    public void setKoordX(long koordX) {
        this.koordX = koordX;
    }

    public long getKoordY() {
        return koordY;
    }

    public void setKoordY(long koordY) {
        this.koordY = koordY;
    }

    public Korisnik getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(Korisnik idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idobaveza != null ? idobaveza.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Obaveza)) {
            return false;
        }
        Obaveza other = (Obaveza) object;
        if ((this.idobaveza == null && other.idobaveza != null) || (this.idobaveza != null && !this.idobaveza.equals(other.idobaveza))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Obaveza[ idobaveza=" + idobaveza + " ]";
    }
    
}
