package is_alarm;

import entities.Alarm;
import entities.Korisnik;
import entities.Pesma;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;


public class Main {
    
    private static EntityManager em = Persistence.createEntityManagerFactory("IS_AlarmPU").createEntityManager();

    @Resource(lookup = "jms/__defaultConnectionFactory")
    private static ConnectionFactory connectionFactory;
    
    @Resource(lookup = "my_alarm_queue")
    private static Queue alarmQueue;
    
    @Resource(lookup = "my_server_topic")
    private static Topic serverTopic;
    
    private static String getDate(Date datum){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.HH:mm:ss");
        return sdf.format(datum);
    }
    
    private static String dohvatiMogucnostiZaAlarm(){
        return "[1] 5s\n"
                + "[2] 30s\n"
                + "[3] 1min\n"
                + "[4] 5min\n"
                + "[5] 30min\n"
                + "[6] 1h"; //"5:30:60:300:1800:3600"
    }
    
    private static void napraviAlarm(int idKorisnik, Date datum, long period){
        Alarm alarm = new Alarm();
        Korisnik korisnik = em.find(Korisnik.class, idKorisnik);
        alarm.setIdKorisnik(korisnik);
        alarm.setVreme(datum);
        alarm.setPeriod(period);
        em.getTransaction().begin();
        em.persist(alarm);
        em.getTransaction().commit();
        
        Timer timer = new Timer();
        if (period == -1){
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    timerRoutine(alarm, korisnik);
                }
              }, datum);
        } else {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    timerRoutine(alarm, korisnik);
                }
              }, datum, period*1000);
        }
    }
    
    private static void timerRoutine(Alarm alarm, Korisnik korisnik){
        String videoId = korisnik.getIdPesma().getUrl().substring(korisnik.getIdPesma().getUrl().indexOf("=")+1); //https://www.youtube.com/watch?v=
        try {
            if (Desktop.isDesktopSupported())
                Desktop.getDesktop().browse(new URI("https://www.youtube.com/embed/" + videoId + "?autoplay=1"));
                //Desktop.getDesktop().browse(new URI(korisnik.getIdPesma().getUrl()));
            System.out.println("Alarm " + alarm.getIdalarm());
        } catch (URISyntaxException | IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
    private static void promeniZvono(int idKorisnik, String zvono){
        Korisnik korisnik = em.find(Korisnik.class, idKorisnik);
        em.getTransaction().begin();
        Pesma pesma = em.find(Pesma.class, Integer.parseInt(zvono));
        korisnik.setIdPesma(pesma);
        em.getTransaction().commit();
    }
    
    private static void posaljiMoguceAlarmeKorisniku(JMSContext context, int idKorisnik){
        try {
            JMSProducer producer = context.createProducer();
            TextMessage textMessage = context.createTextMessage(dohvatiMogucnostiZaAlarm());
            textMessage.setIntProperty("korisnik", idKorisnik);
            textMessage.setStringProperty("tip", "opcijeAlarma");
            producer.send(serverTopic, textMessage);
            
        } catch (JMSException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        JMSContext context = connectionFactory.createContext();
        JMSConsumer consumer = context.createConsumer(alarmQueue);
        
        while(true){
            Message message = consumer.receive();
            if(message instanceof ObjectMessage){
                try {
                    ObjectMessage objectMessage = (ObjectMessage) message;
                    Date datum = (Date) objectMessage.getObject();
                    int idKorisnik = objectMessage.getIntProperty("korisnik");
                    long period = objectMessage.getLongProperty("period");
                                        
                    System.out.println("Primljena je poruka sa sadrzajem: " + getDate(datum));
                    napraviAlarm(idKorisnik, datum, period);
                    
                } catch (JMSException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if(message instanceof TextMessage){
                try {
                    TextMessage textMessage = (TextMessage) message;
                    String text = textMessage.getText();
                    int idKorisnik = textMessage.getIntProperty("korisnik");
                                        
                    System.out.println("Primljena je poruka sa sadrzajem: " + text);
                    if (text.equals("opcijeAlarma"))
                        posaljiMoguceAlarmeKorisniku(context, idKorisnik);
                    else
                        promeniZvono(idKorisnik, text);
                    
                } catch (JMSException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
}
