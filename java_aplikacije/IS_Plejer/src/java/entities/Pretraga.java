/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "pretraga")
@NamedQueries({
    @NamedQuery(name = "Pretraga.findAll", query = "SELECT p FROM Pretraga p"),
    @NamedQuery(name = "Pretraga.findByIdpretraga", query = "SELECT p FROM Pretraga p WHERE p.idpretraga = :idpretraga"),
    @NamedQuery(name = "Pretraga.findByNaziv", query = "SELECT p FROM Pretraga p WHERE p.naziv = :naziv")})
public class Pretraga implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idpretraga")
    private Integer idpretraga;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "naziv")
    private String naziv;
    @JoinColumn(name = "idKorisnik", referencedColumnName = "idKorisnik")
    @ManyToOne(optional = false)
    private Korisnik idKorisnik;

    public Pretraga() {
    }

    public Pretraga(Integer idpretraga) {
        this.idpretraga = idpretraga;
    }

    public Pretraga(Integer idpretraga, String naziv) {
        this.idpretraga = idpretraga;
        this.naziv = naziv;
    }

    public Integer getIdpretraga() {
        return idpretraga;
    }

    public void setIdpretraga(Integer idpretraga) {
        this.idpretraga = idpretraga;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Korisnik getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(Korisnik idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpretraga != null ? idpretraga.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pretraga)) {
            return false;
        }
        Pretraga other = (Pretraga) object;
        if ((this.idpretraga == null && other.idpretraga != null) || (this.idpretraga != null && !this.idpretraga.equals(other.idpretraga))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Pretraga[ idpretraga=" + idpretraga + " ]";
    }
    
}
