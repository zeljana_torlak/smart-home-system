package is_plejer;

import entities.Korisnik;
import entities.Pretraga;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;


public class Main {
    
    private static EntityManager em = Persistence.createEntityManagerFactory("IS_PlejerPU").createEntityManager();

    @Resource(lookup = "jms/__defaultConnectionFactory")
    private static ConnectionFactory connectionFactory;
    
    @Resource(lookup = "my_player_queue")
    private static Queue playerQueue;
    
    @Resource(lookup = "my_server_topic")
    private static Topic serverTopic;
    
    
    private static void reprodukujPesmu(int idKorisnik, String pesma){
        Pretraga pretraga = new Pretraga();
        Korisnik korisnik = em.find(Korisnik.class, idKorisnik);
        pretraga.setIdKorisnik(korisnik);
        pretraga.setNaziv(pesma);
        em.getTransaction().begin();
        em.persist(pretraga);
        em.getTransaction().commit();
        
        System.out.println("Pretraga: " + pesma);
        
        pesma = pesma.replaceAll(" ", "%20");
        try {
            if (Desktop.isDesktopSupported())
                //("https://www.youtube.com/results?search_query=" + pesma + ""));
                Desktop.getDesktop().browse(new URI("https://www.youtube.com/embed?listType=search&list=" + pesma + "&autoplay=1"));
        } catch (URISyntaxException | IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    private static void posaljiPesmeKorisniku(JMSContext context, int idKorisnik){        
        List<Pretraga> pretrage = em.createQuery("SELECT p FROM Pretraga p WHERE p.idKorisnik.idKorisnik = :idKorisnik", Pretraga.class).setParameter("idKorisnik", idKorisnik).getResultList();
        
        StringBuilder sb = new StringBuilder();
        pretrage.forEach((p) -> {
            sb.append(p.getNaziv()).append("\n");
        });
        
        try {
            JMSProducer producer = context.createProducer();
            TextMessage textMessage = context.createTextMessage(sb.toString());
            textMessage.setIntProperty("korisnik", idKorisnik);
            textMessage.setStringProperty("tip", "logPesama");
            producer.send(serverTopic, textMessage);
            
        } catch (JMSException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        JMSContext context = connectionFactory.createContext();
        JMSConsumer consumer = context.createConsumer(playerQueue);
        
        while(true){
            Message message = consumer.receive();
            if(message instanceof TextMessage){
                try {
                    TextMessage textMessage = (TextMessage) message;
                    boolean log = textMessage.getBooleanProperty("log");
                    int idKorisnik = textMessage.getIntProperty("korisnik");
                                        
                    System.out.println("Primljena je poruka sa sadrzajem: " + log);
                    if (log == false){
                        String pesma = textMessage.getText();
                        reprodukujPesmu(idKorisnik, pesma);
                    }
                    else
                        posaljiPesmeKorisniku(context,idKorisnik);
                    
                } catch (JMSException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
}
