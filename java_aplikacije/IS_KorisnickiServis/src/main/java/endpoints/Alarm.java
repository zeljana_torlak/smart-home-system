package endpoints;

import entities.Korisnik;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

@Path("alarm")
@Stateless
public class Alarm {
    
    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;
    
    @Resource(lookup = "jms/__defaultConnectionFactory")
    private ConnectionFactory connectionFactory;
    
    @Resource(lookup = "my_alarm_queue")
    private Queue alarmQueue;

    private Korisnik dohvatiKorisnika(HttpHeaders httpHeaders) {
        List<String> headers = httpHeaders.getRequestHeader("Authorization");
        
        if (headers != null && !headers.isEmpty()){
            String header = headers.get(0);
            String decodedHeader = new String(Base64.getDecoder().decode(header.replaceFirst("Basic ", "")), StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedHeader, ":");
            String username = stringTokenizer.nextToken();
            //String password = stringTokenizer.nextToken();
            
            List<Korisnik> users = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", username).getResultList();
            if (users != null && !users.isEmpty())
                return users.get(0);
        }
        return null;
    }
    
    private String getDate(Date datum){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.HH:mm:ss");
        return sdf.format(datum);
    }
    
    private boolean kreirajAlarm(Korisnik korisnik, Date datum, long period){
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage objectMessage = context.createObjectMessage(datum);
            objectMessage.setIntProperty("korisnik", korisnik.getIdKorisnik());
            objectMessage.setLongProperty("period", period);
            producer.send(alarmQueue, objectMessage);

            return true;
        } catch (JMSException ex) {
            Logger.getLogger(Alarm.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    private boolean posaljiZahtev(Korisnik korisnik, String text){
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            TextMessage textMessage = context.createTextMessage(text);
            textMessage.setIntProperty("korisnik", korisnik.getIdKorisnik());
            producer.send(alarmQueue, textMessage);

            return true;
        } catch (JMSException ex) {
            Logger.getLogger(Alarm.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @POST
    @Path("{datum}")
    public Response kreirajJednokratniAlarm(@Context HttpHeaders httpHeaders, @PathParam("datum") Date datum){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        kreirajAlarm(korisnik, datum, -1);
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Alarm je kreiran: " + getDate(datum)).build();
    }
    
    @POST
    @Path("{datum}/{period}")
    public Response kreirajPeriodicniAlarm(@Context HttpHeaders httpHeaders, @PathParam("datum") Date datum, @PathParam("period") long period){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        kreirajAlarm(korisnik, datum, period);
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Periodicni alarm je kreiran: " + getDate(datum)).build();
    }
    
    @POST
    @Path("zvono/{zvono}")
    public Response postaviZvonoZaKorisnika(@Context HttpHeaders httpHeaders, @PathParam("zvono") int zvono){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        posaljiZahtev(korisnik, "" + zvono);
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Postavljeno zvono: " + zvono).build();
    }
    
    @POST
    @Path("opcije/send")
    public Response posaljiZahtevZaDohvatanjeOpcija(@Context HttpHeaders httpHeaders){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        posaljiZahtev(korisnik, "opcijeAlarma");
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Poslat je zahtev za dohvatanje opcija").build();
    }
    
    @POST
    @Path("opcije/get")
    public Response dohvatiPonudjeneOpcije(@Context HttpHeaders httpHeaders){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        int indeks = MessageReceiverAsync.korisnik2.indexOf(korisnik.getIdKorisnik());
        
        String opcije = "";
        if (indeks != -1){
            opcije = MessageReceiverAsync.sanduce2.remove(indeks);
            MessageReceiverAsync.korisnik2.remove(indeks);
        }
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: opcije: \n" + opcije).build();
    }
}
