package endpoints;

import entities.Korisnik;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Queue;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

@Path("plejer")
@Stateless
public class Plejer {
    
    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;
    
    @Resource(lookup = "jms/__defaultConnectionFactory")
    private ConnectionFactory connectionFactory;
    
    @Resource(lookup = "my_player_queue")
    private Queue playerQueue;

    private Korisnik dohvatiKorisnika(HttpHeaders httpHeaders) {
        List<String> headers = httpHeaders.getRequestHeader("Authorization");
        
        if (headers != null && !headers.isEmpty()){
            String header = headers.get(0);
            String decodedHeader = new String(Base64.getDecoder().decode(header.replaceFirst("Basic ", "")), StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedHeader, ":");
            String username = stringTokenizer.nextToken();
            //String password = stringTokenizer.nextToken();
            
            List<Korisnik> users = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", username).getResultList();
            if (users != null && !users.isEmpty())
                return users.get(0);
        }
        return null;
    }

    private boolean posaljiZahtev(Korisnik korisnik, String text, boolean log){
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            TextMessage textMessage = context.createTextMessage(text);
            textMessage.setIntProperty("korisnik", korisnik.getIdKorisnik());
            textMessage.setBooleanProperty("log", log);
            producer.send(playerQueue, textMessage);

            return true;
        } catch (JMSException ex) {
            Logger.getLogger(Plejer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @POST
    @Path("{pesma}")
    public Response pretraziZadatuPesmu(@Context HttpHeaders httpHeaders, @PathParam("pesma") String pesma){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        posaljiZahtev(korisnik, pesma, false);
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Pretrazena pesma: " + pesma).build();
    }
    
    @POST
    @Path("log/send")
    public Response posaljiZahtevZaDohvatanjePesama(@Context HttpHeaders httpHeaders){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        posaljiZahtev(korisnik, "", true);
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Poslat je zahtev za dohvatanje pesama").build();
    }
    
    @POST
    @Path("log/get")
    public Response dohvatiSvePesme(@Context HttpHeaders httpHeaders){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        int indeks = MessageReceiverAsync.korisnik1.indexOf(korisnik.getIdKorisnik());
        
        String pesme = "\n";
        if (indeks != -1){
            pesme = MessageReceiverAsync.sanduce1.remove(indeks);
            MessageReceiverAsync.korisnik1.remove(indeks);
        }
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: pesme: \n" + pesme).build();
    }
}
