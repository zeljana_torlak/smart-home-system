package endpoints;

import entities.Korisnik;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

@Path("planer")
@Stateless
public class Planer {
    
    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;
    
    @Resource(lookup = "jms/__defaultConnectionFactory")
    private ConnectionFactory connectionFactory;
    
    @Resource(lookup = "my_planner_queue")
    private Queue plannerQueue;

    private Korisnik dohvatiKorisnika(HttpHeaders httpHeaders) {
        List<String> headers = httpHeaders.getRequestHeader("Authorization");
        
        if (headers != null && !headers.isEmpty()){
            String header = headers.get(0);
            String decodedHeader = new String(Base64.getDecoder().decode(header.replaceFirst("Basic ", "")), StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedHeader, ":");
            String username = stringTokenizer.nextToken();
            //String password = stringTokenizer.nextToken();
            
            List<Korisnik> users = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", username).getResultList();
            if (users != null && !users.isEmpty())
                return users.get(0);
        }
        return null;
    }
    
    private String getDate(Date datum){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.HH:mm:ss");
        return sdf.format(datum);
    }
    
    private boolean posaljiZahtevZaDodavanje(Korisnik korisnik, Date pocetak, long trajanje, long koordX, long koordY, int idObaveza){
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage objectMessage = context.createObjectMessage(pocetak);
            objectMessage.setIntProperty("korisnik", korisnik.getIdKorisnik());
            objectMessage.setLongProperty("trajanje", trajanje);
            objectMessage.setLongProperty("koordX", koordX);
            objectMessage.setLongProperty("koordY", koordY);
            objectMessage.setIntProperty("idObaveza", idObaveza);
            producer.send(plannerQueue, objectMessage);

            return true;
        } catch (JMSException ex) {
            Logger.getLogger(Planer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    private boolean posaljiZahtev(Korisnik korisnik, int idObaveza, boolean zahtevZaVremePolaska){
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            TextMessage textMessage = context.createTextMessage();
            textMessage.setIntProperty("korisnik", korisnik.getIdKorisnik());
            textMessage.setIntProperty("idObaveza", idObaveza);
            textMessage.setBooleanProperty("zahtevZaVremePolaska", zahtevZaVremePolaska);
            producer.send(plannerQueue, textMessage);

            return true;
        } catch (JMSException ex) {
            Logger.getLogger(Planer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @POST
    @Path("{pocetak}/{trajanje}/{koordX}/{koordY}")
    public Response dodajObavezu(@Context HttpHeaders httpHeaders, @PathParam("pocetak") Date pocetak, @PathParam("trajanje") long trajanje, @PathParam("koordX") long koordX, @PathParam("koordY") long koordY){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        posaljiZahtevZaDodavanje(korisnik, pocetak, trajanje, koordX, koordY, -1);
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Dodata obaveza " + getDate(pocetak)).build();
    }
    
    @POST
    @Path("{pocetak}/{trajanje}/{koordX}/{koordY}/{idObaveza}")
    public Response izmeniObavezu(@Context HttpHeaders httpHeaders, @PathParam("pocetak") Date pocetak, @PathParam("trajanje") long trajanje, @PathParam("koordX") long koordX, @PathParam("koordY") long koordY, @PathParam("idObaveza") int idObaveza){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        posaljiZahtevZaDodavanje(korisnik, pocetak, trajanje, koordX, koordY, idObaveza);
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Izmenjena obaveza " + idObaveza).build();
    }
    
    @POST
    @Path("uspeh")
    public Response dohvatiUspehOperacije(@Context HttpHeaders httpHeaders){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        int indeks = MessageReceiverAsync.korisnik5.indexOf(korisnik.getIdKorisnik());
        
        String uspeh = "Obaveza uspesno dodata";
        if (indeks != -1){
            MessageReceiverAsync.sanduce5.remove(indeks);
            MessageReceiverAsync.korisnik5.remove(indeks);
            uspeh = "Obaveza se ne moze dodati";
        }
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: " + uspeh).build();
    }
    
    @POST
    @Path("{idObaveza}")
    public Response obrisiObavezu(@Context HttpHeaders httpHeaders, @PathParam("idObaveza") int idObaveza){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        posaljiZahtev(korisnik, idObaveza, false);
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Poslat je zahtev za brisanje obaveze " + idObaveza).build();
    }
    
    @POST
    @Path("send")
    public Response posaljiZahtevZaDohvatanje(@Context HttpHeaders httpHeaders){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        posaljiZahtev(korisnik, -1, false);
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Poslat je zahtev za dohvatanje obaveza").build();
    }
    
    @POST
    @Path("get")
    public Response dohvatiSveObaveze(@Context HttpHeaders httpHeaders){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        int indeks = MessageReceiverAsync.korisnik3.indexOf(korisnik.getIdKorisnik());
        
        String obaveze = "\n";
        if (indeks != -1){
            obaveze = MessageReceiverAsync.sanduce3.remove(indeks);
            MessageReceiverAsync.korisnik3.remove(indeks);
        }
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: obaveze:\n" + obaveze).build();
    }
    
    
    @POST
    @Path("alarm/{idObaveza}")
    public Response zahtevajVremePolaska(@Context HttpHeaders httpHeaders, @PathParam("idObaveza") int idObaveza){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        posaljiZahtev(korisnik, idObaveza, true);
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: Poslat je zahtev za dohvatanje vremena polaska").build();
    }
    
    @POST
    @Path("alarm/get")
    public Response dohvatiVremePolaska(@Context HttpHeaders httpHeaders){
        Korisnik korisnik = dohvatiKorisnika(httpHeaders);
        
        int indeks = MessageReceiverAsync.korisnik4.indexOf(korisnik.getIdKorisnik());
        
        String vremePolaska = "\n";
        if (indeks != -1){
            vremePolaska = MessageReceiverAsync.sanduce4.remove(indeks);
            MessageReceiverAsync.korisnik4.remove(indeks);
        }
        
        return Response.status(Response.Status.OK).entity("[" + korisnik.getKorisnickoIme() + "]: vremePolaska:\n" + vremePolaska).build();
    }
}
