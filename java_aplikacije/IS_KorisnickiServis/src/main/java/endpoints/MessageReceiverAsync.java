package endpoints;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

//Asinhroni prijem poruke koriscenjem message driven bean-a.
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup",
            propertyValue = "my_server_topic"),
    @ActivationConfigProperty(propertyName = "destinationType",
            propertyValue = "javax.jms.Topic")//,
//    @ActivationConfigProperty(propertyName = "subscriptionDurability",
//            propertyValue = "Durable"),
//    @ActivationConfigProperty(propertyName = "subscriptionName",
//            propertyValue = "B")
})
public class MessageReceiverAsync implements MessageListener{
    
    public static LinkedList<String> sanduce1 = new LinkedList<>(); //za pretrage
    public static LinkedList<Integer> korisnik1 = new LinkedList<>();
    
    public static LinkedList<String> sanduce2 = new LinkedList<>(); //za opcije alarma
    public static LinkedList<Integer> korisnik2 = new LinkedList<>();
    
    public static LinkedList<String> sanduce3 = new LinkedList<>(); //za obaveze
    public static LinkedList<Integer> korisnik3 = new LinkedList<>();
    
    public static LinkedList<String> sanduce4 = new LinkedList<>(); //za vremena polaska
    public static LinkedList<Integer> korisnik4 = new LinkedList<>();
    
    public static LinkedList<String> sanduce5 = new LinkedList<>(); //za neuspesnost unosa obaveze
    public static LinkedList<Integer> korisnik5 = new LinkedList<>();

    @Override
    public void onMessage(Message message) {
        if(message instanceof TextMessage){
            try {
                TextMessage textMessage = (TextMessage) message;
                int idKorisnik = textMessage.getIntProperty("korisnik");
                String text = textMessage.getText();
                String tip = textMessage.getStringProperty("tip");
                if (tip.equals("logPesama")){
                    sanduce1.add(text);
                    korisnik1.add(idKorisnik);
                } else if (tip.equals("opcijeAlarma")){
                    sanduce2.add(text);
                    korisnik2.add(idKorisnik);
                } else if (tip.equals("obaveze")){
                    sanduce3.add(text);
                    korisnik3.add(idKorisnik);
                } else if (tip.equals("vremePolaska")){
                    sanduce4.add(text);
                    korisnik4.add(idKorisnik);
                } else if (tip.equals("neuspelaObaveza")){
                    sanduce5.add(text);
                    korisnik5.add(idKorisnik);
                }
                System.out.println("Message received: " + text);
            } catch (JMSException ex) {
                Logger.getLogger(MessageReceiverAsync.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else 
            System.out.println("Message received: none");
    }
    
}
