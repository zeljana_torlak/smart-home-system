package additional;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

@Provider
public class ParameterConverter implements ParamConverterProvider {

    @Override
    public <T> javax.ws.rs.ext.ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] antns) {
        if (rawType.getName().equals(Date.class.getName())){
            return new ParamConverter<T>() {
                @Override
                public T fromString(String value) {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.HH:mm:ss");
                        Date datum = sdf.parse(value);
                        return rawType.cast(datum);
                    } catch (ParseException ex) {
                        Logger.getLogger(ParameterConverter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }

                @Override
                public String toString(T value) {
                    if (value == null)
                        return null;
                    return value.toString();
                }
            };
        }
        return null;
    }
    
}
