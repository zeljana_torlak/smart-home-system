package is_korisnickiuredjaj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IS_KorisnickiUredjaj {

    private static Scanner scanner = new Scanner(System.in);
    private static String username = null;
    private static String password = null;

    private static String urlPart = "";
    private static String hlp;
    
    private static long THREAD_SLEEP_IN_MS = 300;

    private static void callPost() {
        String urlString = "http://localhost:8080/IS_KorisnickiServis/api" + urlPart;
        try {
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            String credentials = username + ":" + password;
            String encodedHeader = Base64.getEncoder().encodeToString(credentials.getBytes("UTF-8"));
            connection.setRequestProperty("Authorization", "Basic " + encodedHeader);
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.connect();

            int responseCode = connection.getResponseCode();
            switch (responseCode) {
                case HttpURLConnection.HTTP_OK:
                    try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                        String readLine;
                        while ((readLine = in.readLine()) != null) {
                            hlp = readLine;
                            System.out.println(readLine);
                        }
                    }
                break;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    username = null;
                    password = null;
                    try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getErrorStream()))) {
                        String readLine;
                        while ((readLine = in.readLine()) != null) {
                            System.out.println(readLine);
                        }
                    }
                    break;
                default:
                    System.out.println("POST NOT WORKED");
                    break;
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(IS_KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(IS_KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IS_KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static String getDate(Date datum){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.HH:mm:ss");
        return sdf.format(datum);
    }

    public static void main(String[] args) {
        while (true) {
            if (username == null) {
                System.out.println("Unesite korisnicko ime:");
                username = scanner.next();
                System.out.println("Unesite sifru:");
                password = scanner.next();
            }

            System.out.println("Izaberite uredjaj\n"
                    + "[1] plejer\n"
                    + "[2] alarm\n"
                    + "[3] planer");
            int opt = scanner.nextInt();

            switch (opt) {
                case 1:
                    playerOpts();
                    break;
                case 2:
                    alarmOpts();
                    break;
                case 3:
                    plannerOpts();
                    break;
                default:
                    System.out.println("Pogresan unos");
                    break;
            }
            urlPart = "";
        }
    }

    private static void playerOpts() {
        urlPart = urlPart + "/plejer";
        System.out.println("Izaberite opciju za plejer\n"
                + "[1] reprodukuj pesmu\n"
                + "[2] pregledaj prethodno reprodukovane pesme");
        int opt = scanner.nextInt();

        switch (opt) {
            case 1:
                player1();
                break;
            case 2:
                player2();
                break;
            default:
                System.out.println("Pogresan unos");
                break;
        }
    }

    private static void alarmOpts() {
        urlPart = urlPart + "/alarm";
        System.out.println("Izaberite opciju za alarm\n"
                + "[1] navij jednokratni alarm\n"
                + "[2] navij periodicni alarm\n"
                + "[3] navij alarm u jednom od ponudjenih trenutaka\n"
                + "[4] konfigurisi zvono alarma");
        int opt = scanner.nextInt();

        switch (opt) {
            case 1:
                alarm1();
                break;
            case 2:
                alarm2();
                break;
            case 3:
                alarm3();
                break;
            case 4:
                alarm4();
                break;
            default:
                System.out.println("Pogresan unos");
                break;
        }
    }

    private static void plannerOpts() {
        urlPart = urlPart + "/planer";
        System.out.println("Izaberite opciju za planer\n"
                + "[1] unesi obavezu\n"
                + "[2] izmeni obavezu\n"
                + "[3] obrisi obavezu\n"
                + "[4] pregledaj obaveze\n"
                + "[5] navij alarm za obavezu");
        int opt = scanner.nextInt();

        switch (opt) {
            case 1:
                planner1();
                break;
            case 2:
                planner2();
                break;
            case 3:
                planner3();
                break;
            case 4:
                planner4();
                break;
            case 5:
                planner5();
                break;
            default:
                System.out.println("Pogresan unos");
                break;
        }
    }

    private static void player1() {
        System.out.println("Unesite pesmu");
        scanner.skip("[\r\n]+");
        scanner.hasNext();
        String pesma = scanner.nextLine();
        pesma = pesma.replaceAll(" ", "%20");

        urlPart = urlPart + "/" + pesma;
        callPost();

    }

    private static void player2() {
        String temp = urlPart;
        urlPart = urlPart + "/log/send";
        callPost();
        
        try {
            Thread.sleep(THREAD_SLEEP_IN_MS);
        } catch (InterruptedException ex) {
            Logger.getLogger(IS_KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
        urlPart = temp + "/log/get";
        callPost();
    }

    private static void alarm1() {
        System.out.println("Unesite datum i vreme u formatu dd.MM.yyyy.HH:mm:ss");
        String datum = scanner.next();
        
        urlPart = urlPart + "/" + datum;
        callPost();        
    }

    private static void alarm2() {
        System.out.println("Unesite datum i vreme u formatu dd.MM.yyyy.HH:mm:ss");
        String datum = scanner.next();
        
        System.out.println("Unesite period u sekundama");
        long trajanje = scanner.nextLong();
        urlPart = urlPart + "/" + datum + "/" + trajanje;
        callPost();   
    }

    private static void alarm3() {
        String temp = urlPart;
        urlPart = urlPart + "/opcije/send";
        callPost();
        
        try {
            Thread.sleep(THREAD_SLEEP_IN_MS);
        } catch (InterruptedException ex) {
            Logger.getLogger(IS_KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
        urlPart = temp + "/opcije/get";
        callPost();
        
        int opt = scanner.nextInt();
        long time = 0;
        switch (opt){
            case 1:
                time = 5;
                break;
            case 2:
                time = 30;
                break;
            case 3:
                time = 60;
                break;
            case 4:
                time = 300;
                break;
            case 5:
                time = 1800;
                break;
            case 6:
                time = 3600;
                break;
            default:
                System.out.println("Pogresan unos");
                return;
        }
        Date datum = new Date((new Date()).getTime() + (time*1000));
        urlPart = "/alarm/" + getDate(datum);
        callPost();
    }

    private static void alarm4() {
        System.out.println("Izaberite jednu od melodija\n"
                + "[1] Dawn Chorus\n"
                + "[2] Nightsky\n"
                + "[3] Morning Flower\n"
                + "[4] Over the Horizon");
        int idPesma = scanner.nextInt();
        
        urlPart = urlPart + "/zvono/" + idPesma;
        callPost();
    }

    private static void planner1() {
        System.out.println("Unesite datum i vreme u formatu dd.MM.yyyy.HH:mm:ss");
        String datum = scanner.next();
        
        System.out.println("Unesite trajanje obaveze");
        long trajanje = scanner.nextLong();
        
        System.out.println("Da li radite od kuce? [Y/N]");
        String c = scanner.next();
        
        long koordX = -1;
        long koordY = -1;
        
        if (c.equals("N")){
            System.out.println("Unesite X koordinatu");
            koordX = scanner.nextLong();

            System.out.println("Unesite Y koordinatu");
            koordY = scanner.nextLong();
        }
        
        urlPart = urlPart + "/" + datum + "/" + trajanje + "/" + koordX + "/" + koordY;
        callPost();
        
        try {
            Thread.sleep(THREAD_SLEEP_IN_MS);
        } catch (InterruptedException ex) {
            Logger.getLogger(IS_KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
        urlPart = "/planer/uspeh";
        callPost();
    }

    private static void planner2() {
        System.out.println("Unesite id obaveze");
        int idObaveza = scanner.nextInt();
        
        String c;
        System.out.println("Da li zelite da promenite datum i vreme pocetka? [Y/N]");
        c = scanner.next();
        
        String datum = "00.00.0000.00:00:00";
        if (c.equals("Y")){
            System.out.println("Unesite datum i vreme u formatu dd.MM.yyyy.HH:mm:ss");
            datum = scanner.next();
        }
        
        System.out.println("Da li zelite da promenite trajanje? [Y/N]");
        c = scanner.next();
        
        long trajanje = -2;
        if (c.equals("Y")){
            System.out.println("Unesite trajanje obaveze");
            trajanje = scanner.nextLong();
        }
        
        System.out.println("Da li zelite da promenite koordinate? [Y/N]");
        c = scanner.next();
        
        long koordX = -2;
        long koordY = -2;
        if (c.equals("Y")){
            System.out.println("Da li radite od kuce? [Y/N]");
            c = scanner.next();

            koordX = -1;
            koordX = -1;  
            if (c.equals("N")){
                System.out.println("Unesite X koordinatu");
                koordX = scanner.nextLong();

                System.out.println("Unesite Y koordinatu");
                koordY = scanner.nextLong();
            }
        }
        
        urlPart = urlPart + "/" + datum + "/" + trajanje + "/" + koordX + "/" + koordY + "/" + idObaveza;
        callPost();
        
        try {
            Thread.sleep(THREAD_SLEEP_IN_MS);
        } catch (InterruptedException ex) {
            Logger.getLogger(IS_KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
        urlPart = "/planer/uspeh";
        callPost();
    }

    private static void planner3() {
        System.out.println("Unesite id obaveze");
        int idObaveza = scanner.nextInt();
        
        urlPart = urlPart + "/" + idObaveza;
        callPost();
    }

    private static void planner4() {
        String temp = urlPart;
        urlPart = urlPart + "/send";
        callPost();
        
        try {
            Thread.sleep(THREAD_SLEEP_IN_MS);
        } catch (InterruptedException ex) {
            Logger.getLogger(IS_KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
        urlPart = temp + "/get";
        callPost();
    }

    private static void planner5() {
        String temp = urlPart;
        System.out.println("Unesite id obaveze");
        int idObaveza = scanner.nextInt();
        
        urlPart = urlPart + "/alarm/" + idObaveza;
        callPost();
        
        try {
            Thread.sleep(THREAD_SLEEP_IN_MS);
        } catch (InterruptedException ex) {
            Logger.getLogger(IS_KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        urlPart = temp + "/alarm/get";
        callPost();
        
        urlPart = "/alarm/" + hlp;
        callPost();
    }

}
